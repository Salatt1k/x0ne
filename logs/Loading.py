import os
import sys
import subprocess
import time
import random
from termcolor import colored

Time = random.randint(1, 3)
Time1 = random.randint(1, 3)
Time2 = random.randint(1, 3)

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_slowly(text):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.030)

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

time.sleep(0.50)

print(colored("[", 'cyan') + colored(".", 'red') + colored("]", 'cyan') + colored(" Загрузка...", 'cyan'))
print(colored("-------------------//", 'red'))

text_to_print = " Выполняется загрузка файлов..."
color = colored("[", 'cyan') + colored("%", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(Time)

text_to_print = " Загрузка атрибутов..."
color = colored("\n[", 'cyan') + colored("/", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(Time1)

text_to_print = " Выполняется компиляция библиотек..."
color = colored("\n[", 'cyan') + colored("#", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(Time2)

text_to_print = " Покдючение к локальному серверу..."
color = colored("\n[", 'cyan') + colored("+", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(Time2)

text_to_print = " Запуск..."
color = colored("\n[", 'cyan') + colored("*", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(0.1)
execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")