import os
import subprocess
import socket
import time
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])
    
def check_internet_connection():
    try:
        socket.create_connection(("8.8.8.8", 53), timeout=5)
    except OSError:
        print(colored("Connection_lost!", 'red'))
        print(colored("------------------//", 'red'))
        print(colored("No internet connection!", 'red'))
        print(colored("Check your internet connection.", 'red'))
        time.sleep(4)
        execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")

check_internet_connection()

def main():
    print(colored("Information Email", 'cyan'))
    print(colored("-------------------//", 'red'))
    email = input(colored("Введите email: ", 'cyan'))
    
    command = "holehe " + email
    os.system(command)
    
if __name__ == "__main__":
    main()
    
En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")