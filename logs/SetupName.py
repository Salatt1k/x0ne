import os
import sys 
import time
import socket
import subprocess
from termcolor import colored

def open_file(file_path):
    subprocess.run(['python', file_path])

def check_internet_connection():
    try:
        socket.create_connection(("8.8.8.8", 53), timeout=5)
    except OSError:
        print(colored("Connection_lost!", 'red'))
        print(colored("------------------//", 'red'))
        print(colored("No internet connection!", 'red'))
        print(colored("Check your internet connection.", 'red'))
        time.sleep(4)
        open_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")

check_internet_connection()

def print_slowly(text):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.030)
        
# Переходим в каталог с файлом blackbird.py
os.chdir("/data/data/com.termux/files/home/x0ne/Programs/blackbird")

# Запрашиваем у пользователя никнейм
print(colored("Search by nickname!", 'cyan'))
print(colored("-------------------//", 'red'))
name = input(colored("Введите никнейм: ", 'cyan'))

text_to_print = " Ищем информацию по никнейму..."
color = colored("[", 'cyan') + colored(">", 'red') + colored("]", 'cyan') + colored(text_to_print, 'cyan')
print_slowly(color)
time.sleep(5)

# Запускаем blackbird.py с указанным никнеймом
os.system(f"python blackbird.py -u {name}")