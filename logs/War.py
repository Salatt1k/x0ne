import subprocess
from termcolor import colored

try:
    print(colored("Warning:", 'red', attrs=['bold']) + colored(" Для использования данной функции установите и включите (Ngrok) на порту (3333) далее можете запускать и использовать скрипт.", 'red', attrs=['bold']))

    print(colored("\nЧто-бы остановить работу сервера, нажмите: (CTRL+C)", 'yellow', attrs=['bold']))

    Enter = input(colored("Для запуска нажмите: (Enter)...", 'yellow', attrs=['bold']))
    if Enter == '':
        script_path = "/data/data/com.termux/files/home/x0ne/Programs/Cam/camdumper.sh"
        subprocess.call(['bash', script_path])

except KeyboardInterrupt:
    print(colored("[", 'cyan', attrs=['bold']) + colored("!", 'red', attrs=['bold']) + colored("]", 'cyan', attrs=['bold']) + colored("Вынужденная остановки работы!", 'cyan', attrs=['bold']))