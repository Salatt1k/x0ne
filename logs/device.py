import psutil
import platform
import socket
import subprocess
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def get_system_info():
    system_info = {
        "Device": platform.node(),
        "OS": platform.system()
    }
    return system_info

print(colored("Данные устройства:", 'cyan'))
print(colored("------------------//", 'red'))

def get_ram_info():
    virtual_memory = psutil.virtual_memory()
    total_ram = virtual_memory.total // (1024 ** 3)  # в гигабайтах
    used_ram = virtual_memory.used // (1024 ** 3)  # в гигабайтах
    free_ram = virtual_memory.free // (1024 ** 3)  # в гигабайтах
    ram_info = {
        "Total RAM": total_ram,
        "Used RAM": used_ram,
        "Free RAM": free_ram
    }
    return ram_info
    

def get_ip_address():
    try:
        # Получение IP-адреса хоста
        ip_address = socket.gethostbyname(socket.gethostname())
        return ip_address
    except:
        return "Не удалось получить IP-адрес"

def get_phone_number():
    try:
        # Получение номера телефона
        output = subprocess.check_output(["adb", "shell", "service", "call", "iphonesubinfo", "1"]).decode().strip()
        phone_number = output.split("'")[1]
        return phone_number
    except:
        return "None"

if __name__ == "__main__":
    system_info = get_system_info()
    print("Device:", system_info["Device"])
    print("Operating System:", system_info["OS"])

    ram_info = get_ram_info()
    for key, value in ram_info.items():
        print(f"{key}: {value} GB")

    ip_address = get_ip_address()
    print("IP-адрес:", ip_address)

    phone_number = get_phone_number()
    print("Phone Number:", phone_number)

print(colored("------------------//", 'red'))
En = input(colored("Нажмите Enter для перезапуска...", 'cyan'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")