import subprocess
import os
import time
import sys
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_slowly(text):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.030)

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

logo = """       ██████╗ ███████╗██████╗
  ██╗  ██╔══██╗██╔════╝██╔══██╗
██████╗██████╔╝█████╗  ██████╔╝
╚═██╔═╝██╔══██╗██╔══╝  ██╔═══╝
  ╚═╝  ██║  ██║███████╗██║
       ╚═╝  ╚═╝╚══════╝╚═╝ """
version = '(8.0)'
stick = '//'
one = '+'

def execute_python_file(file_path):
    subprocess.run(['python', file_path])
    
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

print(colored(logo, 'red'))
print(colored("═══════════════════════════════//", 'red'))
print(colored("Developed by: Salatt1k", 'red'))
print(colored("Telegram: @Salatt1kk", 'red'))
print(colored("Discord: salatt1k_so2", 'red'))
print(colored("-----------------------//", 'red'))
print(colored("\nДобро пожаловать! Пожалуйста ознакомтесь с Readme.txt", 'yellow'))

print("«------------------»")
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("1", 'green') + colored("] Игра", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("2", 'green') + colored("] Калькулятор", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("3", 'green') + colored("] Спам в чат ", 'cyan') + colored("(", 'cyan') + colored("Только PC", 'yellow') + colored(")", 'cyan') + colored(" + ", 'cyan') + colored("(", 'cyan') + colored("Активно", 'green') + colored(")", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("4", 'green') + colored("] Вирус", 'cyan') + colored(" (", 'cyan') + colored("Активно", 'green') + colored(")", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("5", 'green') + colored("] Маскировщик ссылок", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("6", 'green') + colored("] Спам на номер ", 'cyan') + colored("(Активно)", 'green'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("7", 'green') + colored("] Генератор паролей", 'cyan'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("8", 'green') + colored("] Readme.txt", 'cyan'))
print("«------------------»")
print(colored("В разработке:", 'yellow'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("9", 'green') + colored("]", 'cyan') + colored(" Update script ", 'cyan') + colored("(В разработке!)", 'red'))
print(colored("»", 'yellow') + colored("[", 'cyan') + colored("10", 'green') + colored("]", 'cyan') + colored(" Хранилище данных ", 'cyan') + colored("(В разработке!)", 'red'))
print("«------------------»")

print((colored("Текущая версия: GlobalUpdate-", 'green') + colored(version, 'green') + colored(stick, 'green')))
print(colored("═══════════════════════════════", 'green'))
choice = input(colored("Введите:/> ", 'green'))

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

if choice == '1':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/.test.py")
elif choice == '2':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/.Bin.py")
elif choice == '3':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/.Spam.py")
elif choice == '4':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/win.py")
elif choice == '5':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/.httpsmask.py")
elif choice == '6':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/Main/Spavveri.py")
elif choice == '7':
    execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/generation.py")
elif choice == '8':
    with open('/storage/emulated/0/Documents/x0ne/Documentation/Readme.txt', 'r') as file:
        for line in file:
            print(line, end='')
elif choice == '9':
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
        clear_console()
    text_to_print = "Программа находится в разработке, она появится в следующих версиях!"
    color = colored(text_to_print, 'yellow')
    print_slowly(color)
    time.sleep(2)    
    for i in range(1):
        execute_python_file("/storage/emulated/0/Documents/x0ne/logs/llib.py")
elif choice == '10':
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
        clear_console()
    text_to_print = "Программа находится в разработке, она появится в следующих версиях!"
    color = colored(text_to_print, 'yellow')
    print_slowly(color)  
    time.sleep(2)    
    for i in range(1):
        execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/llib.py")
else:
    print(colored("Некорректный выбор.", 'red'))
    print(colored("Выберите сушествующие скрипты...", 'red'))
    time.sleep(2)
    for i in range(1):
        execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/llib.py")