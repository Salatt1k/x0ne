import subprocess
import os
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

print(colored("[", 'cyan') + colored("?", 'red') + colored("]", 'cyan') + colored(" Что вы хотите выбрать?", 'cyan'))
print(colored("-------------------//", 'red'))
print(colored("╭", 'red') + colored("[", 'cyan') + colored("1", 'red') + colored("] Сохранить файл", 'cyan'))
print(colored("╰", 'red') + colored("[", 'cyan') + colored("2", 'red') + colored("] Посмотреть файлы", 'cyan'))
print(colored("-------------------//", 'red'))
op = input(colored("Введите/> ", 'cyan'))

if op == '1':
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
    clear_console()
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/хранилище/Save.py")
elif op == '2':
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
    clear_console()
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/res.py")
else:
    print(colored("Файл не найден!", 'red'))
    for i in range(1):
        execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")
        