import subprocess

# Чтение списка библиотек из файла
with open('/storage/emulated/0/Documents/X0ne/requirements.txt') as f:
    requirements = f.read().splitlines()

# Установка библиотек
for requirement in requirements:
    subprocess.check_call(['pip', 'install', requirement])