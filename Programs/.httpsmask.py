from termcolor import colored
import os
import sys
import time
import subprocess
import urllib.parse

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_show(text, delay=0.04):
    for char in text:
        print(char, end='', flush=True)
        time.sleep(delay)
    print()

def loading_animation(duration, bar_length=20, fill_char='█'):
    start_time = time.time()

    while time.time() - start_time < duration:
        percent = min(100, int((time.time() - start_time) / duration * 100))
        filled_length = int(bar_length * percent / 100)
        bar = fill_char * filled_length + '-' * (bar_length - filled_length)

        sys.stdout.write("\r[{}] {}%".format(bar, percent))
        sys.stdout.flush()
        time.sleep(duration / (bar_length * 2))

    print(colored("\nLoading complete!", 'yellow'))

def mask_link(original_link, service):
    # Кодируем оригинальную ссылку для добавления в параметры запроса
    encoded_original_link = urllib.parse.quote(original_link, safe='')

    # Генерируем маскированную ссылку
    if service.lower() == "1":
        masked_link = f"https://discord.com/redirect?url={encoded_original_link}"
    elif service.lower() == "2":
        masked_link = f"https://www.google.com/url?q={encoded_original_link}"
    elif service.lower() == "3":
        masked_link = f"https://www.youtube.com/redirect?q={encoded_original_link}"
    elif service.lower() == "4":
        masked_link = f"https://www.tiktok.com/uk-UA/?q-{encoded_original_link}"
    else:
        masked_link = original_link
    
    return masked_link

print(colored("-------------------//", 'red'))
# Получаем оригинальную ссылку и выбор сервиса от пользователя
original_link = input(colored("Вставьте оригинальную ссылку: ", 'cyan'))

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

print(colored("[", 'cyan') + colored("!", 'red') + colored("]", 'cyan') + colored(" Выберите сервис для маскирования...", 'cyan'))
print(colored("-------------------//", 'red'))
print(colored("╭", 'red') + colored("[", 'cyan') + colored("1", 'red') + colored("]", 'cyan') + colored(" discord", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("2", 'red') + colored("]", 'cyan') + colored(" google", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("3", 'red') + colored("]", 'cyan') + colored(" youtube", 'cyan'))
print(colored("╰", 'red') + colored("[", 'cyan') + colored("4", 'red') + colored("]", 'cyan') + colored(" TikTok", 'cyan'))
service = input(colored("Введите/> ", 'cyan'))

# Маскируем ссылку
masked_link = mask_link(original_link, service)

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

loading_animation_duration = 1  # Укажите желаемую длительность анимации в секундах
loading_animation(loading_animation_duration, bar_length=25)
# Выводим результат
print(colored("-------------------//", 'red'))

print(colored("Ваша ссылка готова:", 'cyan'), end=' ')
for char in masked_link:
    print(colored(char, 'red'), end='', flush=True)
    time.sleep(0.04)  # Задержка в 0.1 секунды между выводом символов
print()  # Для перехода на новую строку после анимации

En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")