import random
import time
import sys
import subprocess
import os
import datetime
from concurrent.futures import ThreadPoolExecutor
from termcolor import colored
from colorama import Fore, Style
from colorama import init

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_show(text, delay=0.04):
    for char in text:
        print(char, end='', flush=True)
        time.sleep(delay)
    print()

def loading_animation(duration, bar_length=20, fill_char='█'):
    start_time = time.time()

    while time.time() - start_time < duration:
        percent = min(100, int((time.time() - start_time) / duration * 100))
        filled_length = int(bar_length * percent / 100)
        bar = fill_char * filled_length + '-' * (bar_length - filled_length)

        sys.stdout.write("\r[{}] {}%".format(bar, percent))
        sys.stdout.flush()
        time.sleep(duration / (bar_length * 2))

    print(colored("\nLoading complete!", 'yellow'))

logo = """ █████╗ ███████╗██╗  ██╗
██╔════╝ ██╔════╝██║ ██╔╝
██║  ███╗█████╗  █████╔╝ 
██║   ██║██╔══╝  ██╔═██╗ 
╚██████╔╝███████╗██║  ██╗
 ╚═════╝ ╚══════╝╚═╝  ╚═╝
Developed by: XOne //
════════════════════
"""
colored_logo = f"{Fore.MAGENTA}{logo}{Style.RESET_ALL}"
print(colored_logo)
print("<[═════════════⟨•⟩════════════]>")
num1 = random.randint(1, 2)

# Эффект печати при запуске
startup_text = colored("Hello! Let's test your luck.", 'green')
print_show(startup_text)

# Ввод цифры от пользователя
num2 = input(colored("Enter number 1 or 2: ", 'green'))

result_text = colored(f"You entered: {num2}", 'green')
print_show(result_text)

try:
    num2 = int(num2)
    if num2 not in [1, 2]:
        raise ValueError(colored("The entered value is incorrect!", 'red'))
except ValueError as e:
    print(e)
    print(colored("Please enter 1 or 2.", 'red'))
    exit()

if num2 == num1:
    print(colored("You guessed it right, congratulations! Here's your prize:", 'green'))

    # Анимация при выдаче приза
    prize_animation_duration = 5  # Укажите желаемую длительность анимации

    prize_animation_text = colored("🎉 Congratulations! You won cake and wine!🍰🍷", 'green')
    print_show(prize_animation_text, delay=0.1)

    loading_animation(prize_animation_duration)

    print(colored("The cake and wine are ready to serve!🎂🍷", 'green'))
    print("""┈┈┈☆☆☆☆☆☆☆┈┈┈
┈┈╭┻┻┻┻┻┻┻┻┻╮┈┈
┈┈┃╱╲╱╲╱╲╱╲╱┃┈┈
┈╭┻━━━━━━━━━┻╮┈
┈┃╱╲╱╲╱╲╱╲╱╲╱┃┈
┈┗━━━━━━━━━━━┛┈ 
""")

    print(colored("""░░░██████░░░
░░░██████░░░
░░░█▀▀▀▀█░░░
░░░█▄▄▄▄█░░░
░░░██████░░░
░░▄██████▄░░
▄██████████▄
████████████
████████████
████████████
█▀▀▀▀▀██████
█░░░░░██████
█░░░░░██████
█░░░░░██████
█░░░░░██████
█▄▄▄▄▄██████
████████████
████████████
████████████
""", 'white'))
    print(colored("Bon appetit and enjoy your wine!", 'green'))
    exit()
else:
    print(colored("You guessed wrong :(", 'red'))

time.sleep(2)

text_to_print = (colored("The files on your device are being scanned, please wait...", 'red'))
print_show(text_to_print)
time.sleep(2)

loading_animation_duration = 10  # Укажите желаемую длительность анимации в секундах
loading_animation(loading_animation_duration, bar_length=25)

text_to_print = (colored("Ready! File verification complete!", 'green'))
print_show(text_to_print)

text_to_print = (colored("Let's start deleting files...", 'red'))
print_show(text_to_print)

def print_show(text, delay=0.04):
    for char in text:
        print(char, end='', flush=True)
        time.sleep(delay)
    print()
    
text_to_print = ("<[══════════════⟨•⟩═════════════]>")

print_show(text_to_print)

def generate_random_date():
    year = random.randint(2022, 2023)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return datetime.date(year, month, day)

def generate_random_filename():
    prefixes = ["Report", "Code", "Audio", "system", "android"]
    extensions = [".txt", ".jpg", ".jar"]
    filename = f"{random.choice(prefixes)}_{random.randint(100, 999)}"
    extension = random.choice(extensions)
    return f"{filename}{extension}"

def delete_files(num_files):
    file_deletion_times = [random.uniform(0.5, 3.0) for _ in range(num_files)]
    
    with ThreadPoolExecutor(max_workers=num_files) as executor:
        futures = [executor.submit(time.sleep, deletion_time) for deletion_time in file_deletion_times]
        for future in futures:
            future.result()

    for _ in range(num_files):
        file_name = generate_random_filename()
        folder_path = "/storage/emulated/Android/data"  # Замените "/путь/к/удаляемым/файлам" на реальный путь
        path = os.path.join(folder_path, file_name)
        print(f"{colored('Deleted', 'magenta')} 1 {colored('file:', 'magenta')} {colored(file_name, 'magenta')} {colored(generate_random_date(), 'magenta')}, {colored('path:', 'blue')} {colored(path, 'blue')}... ")

for _ in range(15, 45):
    num_files = 1 if random.random() < 0.8 else random.randint(2, 5)
    delete_files(num_files)

init(autoreset=True)

def generate_random_color():
    return random.choice([Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN, Fore.WHITE])

def fake_lag_effect(rows, columns, symbol_size, duration):
    for _ in range(duration):
        screen = []
        for _ in range(rows):
            row = [generate_random_color() + '█' * symbol_size for _ in range(columns // symbol_size)]
            screen.append(''.join(row))
        
        print('\n'.join(screen))
        time.sleep(0.1)
        print('\3c')  # Clear screen

if __name__ == "__main__":
    rows = 1000
    columns = 60
    symbol_size = 35  # Increase this value to make symbols larger
    duration = 2277333277667727  # Number of frames

    fake_lag_effect(rows, columns, symbol_size, duration)
    
En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")