import phonenumbers
import sys
import os
import time
import subprocess
from phonenumbers import carrier, geocoder, timezone
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    
clear_console()

print(colored("Пробив номера...", 'cyan'))
print(colored("------------------//", 'red'))
number = input(colored("Введите номер телефона/> ", 'cyan'))

num = phonenumbers.parse(number)
valid = phonenumbers.is_valid_number(num)
zone = timezone.time_zones_for_number(num)
cari = carrier.name_for_number(num, 'ru')
region = geocoder.description_for_number(num, 'ru')

if valid == True:
    x = print(colored("Сушествование номера: Подтверждено!", 'cyan'))
elif valid == False:
    x = print(colored("Сушествование номера: Не подтверждено!", 'red'))

print(colored("Зона: ", 'cyan') + colored("".join(zone), 'cyan'))
print(colored("Оператор: ", 'cyan') + colored("".join(cari), 'cyan'))
print(colored("Регион: ", 'cyan') + colored("".join(region), 'cyan'))
print(colored("Номер: ", 'cyan') + colored(x, 'cyan'))

En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")