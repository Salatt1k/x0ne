import socket
import threading

def receive_messages(client_socket):
    while True:
        try:
            data = client_socket.recv(1024)
            if data:
                print(data.decode())
            else:
                print("Disconnected from server")
                break
        except ConnectionResetError:
            print("Disconnected from server")
            break

def start_client():
    server_ip = input("Введите IP-адрес сервера: ")
    server_port = input("Введите порт сервера: ")

    try:
        server_port = int(server_port)
    except ValueError:
        print("Порт должен быть целым числом.")
        return

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_ip, server_port))

    nickname = input("Введите ваш ник: ")
    client_socket.sendall(nickname.encode())

    receive_thread = threading.Thread(target=receive_messages, args=(client_socket,))
    receive_thread.start()

    while True:
        message = input()
        client_socket.sendall(message.encode())

start_client()