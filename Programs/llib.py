import subprocess
import os
import time
import socket
import sys
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_slowly(text):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.030)

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

def check_internet_connection():
    try:
        socket.create_connection(("8.8.8.8", 53), timeout=5)
    except OSError:
        print(colored("Connection_lost!", 'red'))
        print(colored("------------------//", 'red'))
        print(colored("No internet connection!", 'red'))
        print(colored("Check your internet connection.", 'red'))
        time.sleep(4)
        execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")

logo = """       ██████╗ ███████╗██████╗
  ██╗  ██╔══██╗██╔════╝██╔══██╗
██████╗██████╔╝█████╗  ██████╔╝
╚═██╔═╝██╔══██╗██╔══╝  ██╔═══╝
  ╚═╝  ██║  ██║███████╗██║
       ╚═╝  ╚═╝╚══════╝╚═╝ """
version = '(9.0.2)'
stick = '//'
one = '+'
txt1 = "\nВы открыли файл: (Игра.py) \n═══════════════════════════════//"
txt2 = "\nВы открыли файл: (Калькулятор.py) \n═══════════════════════════════//"
txt3 = "\nВы открыли файл: (Спам в чат.py)\n═══════════════════════════════//"
txt4 = "\nВы открыли файл: (Вирус.py)\n═══════════════════════════════//"
txt5 = "\nВы открыли файл: (Маскировщик ссылок.py)\n═══════════════════════════════//"
txt6 = "\nВы открыли файл: (Спам на номер.py)\n═══════════════════════════════//"
txt7 = "\nВы открыли файл: (Генератор паролей.py)\n═══════════════════════════════//"
txt8 = "\nВы открыли файл: (Журнал аудита.py)\n═══════════════════════════════//"
txt9 = "\nВы открыли файл который находится стадии разработки!\n═══════════════════════════════//"
txt10 = "\nВы открыли файл: (Хранилище.py)\n═══════════════════════════════//"
txt11 = "\nВы открыли файл: (Оперативная память.py)\n═══════════════════════════════//"
txt12 = "\nВы открыли файл: (Публичный чат.py)\n═══════════════════════════════//"
txt13 = "\nВы открыли файл: (Проверка реигистрации.py)\n═══════════════════════════════//"
txt14 = "\nВы открыли файл: (Консоль.py)\n═══════════════════════════════//"
txt15 = "\nВы открыли файл: (Пробив по номеру.py)\n═══════════════════════════════//"
txt16 = "\nВы открыли файл: (Пробив по nickname.py)\n═══════════════════════════════//"
txt17 = "\nВы открыли файл: (Скам сайт.py)\n═══════════════════════════════//"
txt18 = "\nВы открыли файл: (Геолокация.py)\n═══════════════════════════════//"
txt19 = "\nВы открыли файл: (Геолокация.py)\n═══════════════════════════════//"

def execute_python_file(file_path):
    subprocess.run(['python', file_path])
    
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

print(colored(logo, 'red'))
print(colored("═══════════════════════════════//", 'red'))
print(colored("Version = (11.1.2)", 'red'))

print(colored("-------------------//", 'red'))
print(colored("╭", 'red') + colored("[", 'cyan') + colored("1", 'red') + colored("] Игра", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("2", 'red') + colored("] Калькулятор", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("3", 'red') + colored("] Спам в чат (PC)", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("4", 'red') + colored("] Вирус", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("5", 'red') + colored("] Маскировщик ссылок", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("6", 'red') + colored("] Спам на номер ", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("7", 'red') + colored("] Генератор паролей", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("8", 'red') + colored("] Журнал аудита", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("9", 'red') + colored("] Хранилище секретных файлов", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("10", 'red') + colored("] 0бновить скрипт", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("11", 'red') + colored("] Публичный чат", 'cyan') + colored(" (Beta)", 'red'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("12", 'red') + colored("] Пробить реигистрацию", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("13", 'red') + colored("] Пробив по номеру", 'cyan') + colored(" (Новое)", 'red'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("14", 'red') + colored("] Консоль", 'cyan') + colored(" (Beta)", 'red'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("15", 'red') + colored("] Пробив по Nickname", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("16", 'red') + colored("] Фишинг сайт", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("17", 'red') + colored("] Пробить геолокацию", 'cyan'))
print(colored("├", 'red') + colored("[", 'cyan') + colored("18", 'red') + colored("] Взлом веб-камеры", 'cyan') + colored(" (Beta)", 'red'))
print(colored("╰", 'red') + colored("[", 'cyan') + colored("19", 'red') + colored("] Оперативная память", 'cyan'))
print(colored("-------------------//", 'red'))

choice = input(colored("-", 'red') + colored("[", 'cyan') + colored("/", 'red') + colored("]", 'cyan') + (colored(" Введите/> ", 'cyan')))

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

if choice == '1':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt1)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/.test.py")
elif choice == '2':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt2)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/.Bin.py")
elif choice == '3':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt3)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/.Spam.py")
elif choice == '4':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt4)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/delete/.Virus.py")
elif choice == '5':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt5)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/.httpsmask.py")
elif choice == '6':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt6)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/Main/Spamnum.py")
elif choice == '7':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt7)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/generation.py")
elif choice == '8':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt8)
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'r') as file:
        open = file.read()
        print(open)
        print(colored("\nНажмите (Enter) для принятия перезапуска...", 'yellow'))
        input()
        for i in range(1):
            execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")
elif choice == '9':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt10)
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/results.py")
elif choice == '10':
    script_path = "/data/data/com.termux/files/home/x0ne/Update.sh"
    subprocess.call(['bash', script_path])
elif choice == '11':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt11)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/chat.py")
elif choice == '12':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt13)
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/pip.py")
elif choice == '13':
    check_internet_connection()
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt15)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/open_file.py")
elif choice == '14':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt14)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/Console.py")
elif choice == '15':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt14)
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/SetupName.py")
elif choice == '16':
    check_internet_connection()
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/warning.py")
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt17)
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/website.py")
elif choice == '17':
    check_internet_connection()
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/warn.py")
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt18)
elif choice == '18':
    check_internet_connection()
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/War.py")
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt19)
elif choice == '19':
    with open("/data/data/com.termux/files/home/x0ne/logs/Auditfile.txt", 'a') as file:
        file.write(txt6)
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/device.py")
else:
    print(colored("Некорректный выбор.", 'red'))
    print(colored("Выберите сушествующие скрипты...", 'red'))
    time.sleep(2)
    for i in range(1):
        execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")