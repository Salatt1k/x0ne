import random
import string
import time
import subprocess
import sys
import os
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def loading_animation(duration, bar_length=20, fill_char='█'):
    start_time = time.time()

    while time.time() - start_time < duration:
        percent = min(100, int((time.time() - start_time) / duration * 100))
        filled_length = int(bar_length * percent / 100)
        bar = fill_char * filled_length + '-' * (bar_length - filled_length)

        sys.stdout.write("\r[{}] {}%".format(bar, percent))
        sys.stdout.flush()
        time.sleep(duration / (bar_length * 2))

    print(colored("\nLoading complete!", 'cyan'))

def generate_password(length):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for _ in range(length))
    return password
    
print(colored("-------------------//", 'red'))

# Запрос у пользователя длины пароля
length = int(input(colored("Введите желаемую длину пароля: ", 'cyan')))

loading_animation_duration = 0.50  # Укажите желаемую длительность анимации в секундах
loading_animation(loading_animation_duration, bar_length=25)

# Проверка, что введенное значение длины положительное
while length <= 0:
    print("Длина пароля должна быть положительным числом.")
    length = int(input("Введите желаемую длину пароля: "))

# Генерация пароля и вывод на экран
password = generate_password(length)
print(colored("Сгенерированный пароль: ", 'cyan') + str(password))

En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")