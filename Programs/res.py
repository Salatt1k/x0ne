import os
import subprocess
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def list_files(directory):
    """
    Выводит список файлов в заданной директории.
    
    :param directory: Путь к директории.
    """
    print(colored(f"Ваш список сохраненных файлов:", 'cyan'))
    print(colored("--------------------------------//", 'red'))
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        if os.path.isfile(file_path):
            print(colored(filename, 'cyan'))

def main():
    directory = "/data/data/com.termux/files/home/x0ne/Programs/хранилище"
    list_files(directory)

if __name__ == "__main__":
    main()
    
En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")