import os
import shutil
import subprocess
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def save_file(file_path, destination):
    """
    Сохраняет файл из указанного пути в указанное место.
    
    :param file_path: Путь к файлу, который нужно сохранить.
    :param destination: Путь, куда нужно сохранить файл.
    """
    if not os.path.exists(destination):
        os.makedirs(destination)
    shutil.copy(file_path, destination)
    print(colored(f"Файл {file_path} успешно сохранен в {destination}", 'green'))

def main():
    print(colored("[", 'cyan') + colored("!", 'red') + colored("]", 'cyan') + colored(" Внимание! Функция лишь копирует файл с введенной вами директории, поэтому рекомендуется удалить оригинальный файл со старой директории...", 'cyan'))
    print(colored("═══════════════════════════════//", 'red'))
    file_path = input(colored("[", 'cyan') + colored("/", 'red') + colored("]", 'cyan') + colored(" Введите путь к файлу: ", 'cyan'))
    destination = '/data/data/com.termux/files/home/x0ne/Programs/хранилище'
    save_file(file_path, destination)

if __name__ == "__main__":
    main()

En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")