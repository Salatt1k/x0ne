import os
import time
import sys
import subprocess
import socket
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def list_files(directory):
    """
    Выводит список файлов в заданной директории.
    
    :param directory: Путь к директории.
    """
    print(colored("    <", 'red') + colored("dir", 'cyan') + colored(">", 'red'))
    print(colored("All_files:", 'cyan'))
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        if os.path.isfile(file_path):
            print(colored(filename, 'cyan'))

def main():
    directory = "/data/data/com.termux/files/home/x0ne/Programs/хранилище"
    list_files(directory)

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    
def open_file(file_path):
    subprocess.run(['python', file_path])

print(colored("Powered by Salatt1k", 'cyan'))
print(colored("--------------------//", 'red'))

while True:
    console = input(colored("</", 'red') + colored("console", 'cyan') + colored(">", 'red'))
    
    if console == '</help>':
        print(colored("    <", 'red') + colored("help", 'cyan') + colored(">", 'red'))
        print(colored('All_commands:', 'cyan'))
        print(colored("</help>", 'cyan') + colored(" Показывает список комманд", 'cyan'))
        print(colored("</clear>", 'cyan') + colored(" Очищает консоль", 'cyan'))
        print(colored("</close>", 'cyan') + colored(" Выход из консоли", 'cyan'))
        print(colored("</info>", 'cyan') + colored(" Показывает информацию о консоли", 'cyan'))
        print(colored("</ip>", 'cyan') + colored(" Показывает ваш ip адрес", 'cyan'))
        print(colored("</data>", 'cyan') + colored(" Показывает время", 'cyan'))
        print(colored("</update>", 'cyan') + colored(" Обновляет программу", 'cyan'))
        print(colored("</open>", 'cyan') + colored(" Открывает файл", 'cyan'))
        print(colored("</cd>", 'cyan') + colored(" Выход из директории", 'cyan'))
        print(colored("</dir>", 'cyan') + colored(" Показывает все файлы хранилища", 'cyan'))
        print(colored("</exit>", 'cyan') + colored(" Выход из программы", 'cyan'))
        print(colored("</reboot>", 'cyan') + colored(" Перезапускает программу", 'cyan'))
        print(colored("    <", 'red') + colored("help", 'cyan') + colored(">", 'red'))
    elif console == '</clear>':
        clear_console()
    elif console == '</ip>':
        print(colored("    <", 'red') + colored("commands", 'cyan') + colored(">", 'red'))
        print(colored("       <", 'red') + colored("ip", 'cyan') + colored(">", 'red'))
        print(colored("       You_IP: 127.0.0.1", 'cyan'))
        print(colored("       <", 'red') + colored("ip", 'cyan') + colored(">", 'red'))
        print(colored("    <", 'red') + colored("commands", 'cyan') + colored(">", 'red'))
    elif console == '</update>':
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red'))
        script_path = "/data/data/com.termux/files/home/x0ne/Update.sh"
        subprocess.call(['bash', script_path])
        print(colored("The update has been installed successfully! Please enter the command </reboot> to restart the program.", 'cyan'))
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red'))
    elif console == '</close>':
        clear_console()
        open_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")
    elif console == '</info>':
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red'))
        print(colored("        <", 'red') + colored("info", 'cyan') + colored(">", 'red'))
        print(colored("Console_information:", 'cyan'))
        print(colored("Version_Programs: 11.1.2", 'cyan'))
        print(colored("Version_Console: 0.5.5", 'cyan'))
        print(colored("        <", 'red') + colored("info", 'cyan') + colored(">", 'red'))
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red'))
    elif console == '</dir>':
        if __name__ == "__main__":
            main()
        print(colored("    <", 'red') + colored("dir", 'cyan') + colored(">", 'red'))
    elif console == '</data>':
        print(colored("    <", 'red') + colored("data", 'cyan') + colored(">", 'red'))
        t = time.localtime(time.time())
        localtime = time.asctime(t)
        str = "Your current date: " + time.asctime(t)
        col = colored(str, 'cyan')
        print(col)
        print(colored("    <", 'red') + colored("data", 'cyan') + colored(">", 'red'))
    elif console == '</exit>':
        exit()
    elif console == '</open>':
        open = input(colored("</", 'red') + colored("open", 'cyan') + colored(">", 'red'))
        open_file(open)
        if open == '</cd>':
            open_file("/data/data/com.termux/files/home/x0ne/Programs/Console.py")
    elif console == '</reboot>':
        open_file("/data/data/com.termux/files/home/x0ne/setup.py")
    else:
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red') + colored("[", 'red') + colored(console, 'red') + colored("]", 'red') + colored(" No commands...", 'red'))
        print(colored("Enter the command </help> to display a list of commands!", 'red'))
        print(colored("    <", 'red') + colored("console", 'cyan') + colored(">", 'red'))