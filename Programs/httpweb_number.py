# -*- coding UTF-8 -*-
# ИНФОРМАЦИЯ БЫЛА ПОЛУЧЕНА ОТ HTMLWEB.RU
# NOBLACK-MAIL НИКАК НЕ УЧАСТВУЕТ В СОХРАНЕНИИ ПОЛЬЗОВАТЕЛЬСКИХ ДАННЫХ!.
# DATE: Вторник, 18 Апреля 2023 г. 03:39:15 (+03)

import requests
import subprocess
from termcolor import colored
from config import COLOR_CODE, console_clear
from phoneradar_ru import PhoneRadar
from functools import lru_cache

def open_file(file_path):
    subprocess.run(['python', file_path])

class HttpWebNumber:
    """Информация получаемая с сайта httpweb.ru никак\n
    не синхронизирован/связан с noblack-mail а так-же с noblack.command.
    :param: `user_number` - Номер телефона."""

    def __init__(self) -> None:
        self.__check_number_link: str = "https://htmlweb.ru/geo/api.php?json&telcod="
        self.__not_found_text: str = "Информация отсутствует"

    # Получение данных по номеру
    @lru_cache(maxsize=None)
    def __return_number_data(self, user_number: str) -> dict:
        """Получение данных о номере телефона
        :param: `self.__check_mnp_link:` str — Адрес сайта для получения данных по номеру телефона.
        :return: `__result_number_data:` dict - Данные клиента.
        """

        # Получение данных MNP по номеру тел. клиента 
        try:
            __result_number_data = requests.get(self.__check_number_link + user_number)
        
            if __result_number_data.ok:
                __result_number_data: dict = __result_number_data.json()

            else:
                __result_number_data: dict = {"status_error": True}

        except requests.exceptions.ConnectionError as connection_error:
            __result_number_data: dict = {"status_error": True}
        
        return __result_number_data

    # Ввод данных в консоль
    @property
    def print_number_results(self) -> str:
        """Вывод полученных данных"""

        try:
            console_clear()
            _user_number: str = input(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[{COLOR_CODE["RED"]}1{COLOR_CODE["CYAN"]}] {COLOR_CODE["CYAN"]}' + 
                f'Введите номер телефона{COLOR_CODE["CYAN"]}: {COLOR_CODE["RESET"]}').strip()
            
            # Если ввели номер телефона
            if _user_number:
                print(f'{COLOR_CODE["BOLD"]}{COLOR_CODE["CYAN"]}[{COLOR_CODE["RED"]}~{COLOR_CODE["CYAN"]}]{COLOR_CODE["CYAN"]} Поиск данных... {COLOR_CODE["RESET"]}\n')
                _get_user_number_data = self.__return_number_data(user_number=_user_number)

                # Проверка статуса ошибки
                limit = _get_user_number_data.get("limit")
                if limit is not None and limit <= 0:
                    print(f'\n{COLOR_CODE["RED"]}{COLOR_CODE["BOLD"]}[!] '+
                    f'{COLOR_CODE["RESET"]}К сожалению, вы израсходовали {COLOR_CODE["DARK"]}все лимиты')
                    print(f'{COLOR_CODE["RED"]}{COLOR_CODE["BOLD"]}[+] '+
                    f'{COLOR_CODE["RESET"]}Всего лимитов: {COLOR_CODE["DARK"]}'+
                    f'{str(_get_user_number_data.get("limit", self.__not_found_text))}{COLOR_CODE["RESET"]}')
                
                elif _get_user_number_data.get("status_error") or _get_user_number_data.get("error"):
                    print(f'{COLOR_CODE["RED"]}[!] {COLOR_CODE["YELLOW"]}Данные не найдены {COLOR_CODE["RESET"]}\n')


                # Ввод данных о номере
                else:
                    _number_data_unknown = _get_user_number_data
                    _number_data_country = _get_user_number_data.get('country')
                    _number_data_capital = _get_user_number_data.get('capital')
                    _number_data_region = _get_user_number_data.get('region')
                    _number_data_other = _get_user_number_data.get('0')

                    if not _number_data_region:
                        _number_data_region: dict = {"autocod": self.__not_found_text, 
                        "name": self.__not_found_text,
                        "okrug": self.__not_found_text}

                    # Страна            
                    # Для украины информация о стране отсутствует
                    if _number_data_country.get("country_code3") == 'UKR':
                        print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                            f'{COLOR_CODE["CYAN"]}Страна:{COLOR_CODE["F_CL"]} Украина{COLOR_CODE["RESET"]}')
                    
                    else:
                        print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                            f'{COLOR_CODE["CYAN"]}Страна:{COLOR_CODE["F_CL"]} '+
                            f'{_number_data_country.get("name", self.__not_found_text)}, ' +
                            f'{_number_data_country.get("fullname", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Город
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Город:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_other.get("name", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Почтовый индекс
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Почтовый индекс:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_other.get("post", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Код валюты
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Код валюты:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_country.get("iso", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Телефонные коды 
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Телефонные коды:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_capital.get("telcod", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Посмотреть в wiki
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Посмотреть в wiki:{COLOR_CODE["RESET"]}{COLOR_CODE["URL_L"]}{COLOR_CODE["UNDERLINE"]} '+
                        f'{_number_data_other.get("wiki", self.__not_found_text)}{COLOR_CODE["RESET"]}')


                    # Регион номереа авто
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Гос. номер региона авто:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_region.get("autocod", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Оператор
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Оператор:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_other.get("oper", self.__not_found_text)}, {COLOR_CODE["DARK"]}'+
                            f'{_number_data_other.get("oper_brand", self.__not_found_text)}, '+
                            f'{_number_data_other.get("def", self.__not_found_text)}{COLOR_CODE["RESET"]}')
                
                    # Местоположение
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Местоположение:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_country.get("name", self.__not_found_text)}, ' +
                        f'{_number_data_region.get("name", self.__not_found_text)}, ' +
                        f'{_number_data_other.get("name", self.__not_found_text)}{COLOR_CODE["DARK"]} ('+
                            f'{_number_data_other.get("latitude", self.__not_found_text)}, '+
                            f'{_number_data_other.get("longitude", self.__not_found_text)}){COLOR_CODE["RESET"]}')

                    # Открыть на карте (google) 
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Открыть на карте (google):{COLOR_CODE["RESET"]}{COLOR_CODE["URL_L"]}{COLOR_CODE["UNDERLINE"]} '+
                        f'https://www.google.com/maps/place/'+
                        f'{_number_data_other.get("latitude", self.__not_found_text)}+'+
                        f'{_number_data_other.get("longitude", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Локация 
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Локация:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_unknown.get("location", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Язык
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Язык общения:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_country.get("lang", self.__not_found_text).title()}, '+
                            f'{_number_data_country.get("langcod", self.__not_found_text)}{COLOR_CODE["RESET"]}')
            
                    # Край/Округ/Область
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Край/Округ/Область:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_region.get("name", self.__not_found_text)}, '+ 
                            f'{_number_data_region.get("okrug", self.__not_found_text)}{COLOR_CODE["RESET"]}')                     

                    # Столица
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Столица:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_capital.get("name", self.__not_found_text)}{COLOR_CODE["RESET"]}')

                    # Широта/Долгота
                    print(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["CYAN"]}Широта/Долгота:{COLOR_CODE["F_CL"]} '+
                        f'{_number_data_other.get("latitude", self.__not_found_text)}, '+
                        f'{_number_data_other.get("longitude", self.__not_found_text)}{COLOR_CODE["RESET"]}')
                    
                    # Всего лимитов
                    print(f'\n{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[+] '+
                        f'{COLOR_CODE["RED"]}Всего лимитов: '+
                        f'{str(_get_user_number_data.get("limit", self.__not_found_text))}{COLOR_CODE["RESET"]}')
                        
                    Enter = input(colored("\nДля выхода нажмите: (Enter)...", 'yellow', attrs=['bold']))
                    
                    if Enter == '':
                        open_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")
   
                    input(f'{COLOR_CODE["CYAN"]}{COLOR_CODE["BOLD"]}[{COLOR_CODE["RED"]}!{COLOR_CODE["CYAN"]}] {COLOR_CODE["LI_G"]}' + 
                        f'Чтобы завершить поиск, нажмите{COLOR_CODE["DARK"]} {COLOR_CODE["RESET"]}ENTER ')
            
            # Если не ввели номер телефона
            else:
                print(f'{COLOR_CODE["RED"]}[!] {COLOR_CODE["YELLOW"]}Ошибка, введите номер телефона! {COLOR_CODE["RESET"]}\n')

        except KeyboardInterrupt:
            print(f'\n{COLOR_CODE["RED"]}[!] {COLOR_CODE["RED"]}Вынужденная остановка работы! {COLOR_CODE["RESET"]}\n')
            
if __name__ == "__main__":
    HttpWebNumber().print_number_results