from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        h1 {
            color: #333;
            text-align: center;
        }
    </style>
    <script>
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(sendLocation, handleLocationError);
            } else {
                alert("Геолокация не поддерживается вашим браузером.");
            }
        }

        function sendLocation(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            // Отправляем данные о местоположении на сервер
            fetch('/submit_location', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({latitude: latitude, longitude: longitude})
            })
            .then(response => {
                if (response.ok) {
                    console.log('Местоположение успешно отправлено на сервер.');
                } else {
                    console.error('Ошибка при отправке местоположения на сервер.');
                }
            });
        }

        function handleLocationError(error) {
            console.error('Ошибка при получении геолокации:', error.message);
        }

        getLocation(); // Вызываем функцию при загрузке страницы
    </script>
</head>
<body>
    <h1>Перед тем как войти на сайт, включите геолокацию, и обновите страницу попутно разрешая доступ к ней.</h1>
</body>
</html>
"""

@app.route('/submit_location', methods=['POST'])
def submit_location():
    data = request.get_json()
    latitude = data['latitude']
    longitude = data['longitude']
    print("Местоположение:", latitude, longitude)
    return 'Местоположение успешно получено и обработано.'

if __name__ == '__main__':
    app.run(debug=True)