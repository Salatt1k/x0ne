import sys
import time
import subprocess
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def print_show(text, delay=0.04):
    for char in text:
        print(char, end='', flush=True)
        time.sleep(delay)
    print()

def loading_animation(duration, bar_length=20, fill_char='█'):
    start_time = time.time()

    while time.time() - start_time < duration:
        percent = min(100, int((time.time() - start_time) / duration * 100))
        filled_length = int(bar_length * percent / 100)
        bar = fill_char * filled_length + '-' * (bar_length - filled_length)

        sys.stdout.write("\r[{}] {}%".format(bar, percent))
        sys.stdout.flush()
        time.sleep(duration / (bar_length * 2))

    print(colored("\nLoading complete!", 'cyan'))

print(colored("-------------------//", 'red'))

a = int(input(colored("Введите цифру/число: ", 'cyan')))
b = int(input(colored("Введите вторую цифру/число: ", 'cyan')))

try:
    operator = input(colored("Введите (+, -, *, /): ", 'red'))
    if operator not in ['+', '-', '*', '/']:
        raise ValueError(colored("Не верно введенное действие!", 'red'))
except ValueError as e:
    print(e)
    exit()
    
loading_animation_duration = 1  # Укажите желаемую длительность анимации в секундах
loading_animation(loading_animation_duration, bar_length=25)
    
print(colored("-------------------//", 'red'))
    
if operator == '+':
    result = (a + b)
    print(colored("result: " + str(result), 'cyan'))
    
if operator == '-':
    result = (a - b)
    print(colored("result: " + str(result), 'cyan'))
    
if operator == '*':
    result = (a * b)
    print(colored("result: " + str(result), 'cyan'))
    
if operator == '/':
    result = (a / b)
    print(colored("result: " + str(result), 'cyan'))
    
En = input(colored("\nНажмите Enter для перезапуска...", 'yellow'))

if En == '':
    execute_python_file("/data/data/com.termux/files/home/x0ne/Programs/llib.py")