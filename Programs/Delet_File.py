import shutil

def delete_files(path):
    try:
        # Удаляем все файлы и подпапки в указанной директории
        shutil.rmtree(path)
        print(f"Все файлы и подпапки в {path} успешно удалены.")
    except Exception as e:
        print(f"Ошибка при удалении: {e}")

# Указываем путь к папке, файлы в которой нужно удалить
path_to_delete = "/storage/emulated/0/Documents/x0ne"
delete_files(path_to_delete)