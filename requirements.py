import subprocess

# Чтение списка библиотек из файла
with open('/data/data/com.termux/files/home/x0ne/requirements.txt') as f:
    requirements = f.read().splitlines()

# Установка библиотек
for requirement in requirements:
    subprocess.check_call(['pip', 'install', requirement])
