#!/bin/bash

# Переходим в домашнюю директорию
cd 

# Удаляем старую версию скрипта, если она существует
rm -rf x0ne

# Клонируем последнюю версию скрипта из репозитория GitHub
git clone https://gitlab.com/Salatt1k/x0ne
cd x0ne
python setup.py
  