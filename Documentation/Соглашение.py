import time
import sys
import os
import subprocess
from termcolor import colored

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

print(colored("Пользовательское соглашение!", 'cyan'))
print(colored("----------------------------//", 'red'))
print(colored("\nДанная программа создана в ознакомительных целях, все действия вы делаете на свой страх и риск!", 'cyan'))
print(colored("\nРазработчик не несет никакой ответсвенности за последствия использования данной программы, и не следует ее применять в различных конфликтах.", 'cyan'))
print(colored("\nИспользуйте данную программу с осторожностью, и уважайте личное мнение и пространство других людей.", 'cyan'))
print(colored("\nИспользование данной программы может нести серьезные для вас проблемы, а значит не следует использовать эту программу для вреда устройства другого человека.", 'cyan'))
print(colored("\nВнимание! Мы не собираем, и не используем ваши данные...", 'cyan'))
print(colored("\nТак-же не пытайтесь модифицировать, изменять, или продавать программу. Иначе вам перестанут выдавать обновления!", 'cyan'))

def wait_for_enter():
    print(colored("\nНажмая (Enter) вы автоматически принимаете соглашение!", 'cyan'))
    print(colored("----------------------------//", 'red'))
    print(colored("Нажмите (Enter) для принятия соглашения...", 'yellow'))
    input()

if __name__ == "__main__":
    wait_for_enter()
    execute_python_file("/data/data/com.termux/files/home/x0ne/logs/Loading.py")