import os
import subprocess
from termcolor import colored

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

print(colored("[", 'cyan') + colored("?", 'red') + colored("]", 'cyan') + colored(" Вы хотите посетить наш телеграм канал?", 'cyan'))
print(colored("-------------------//", 'red'))
print(colored("╭", 'red') + colored("[", 'cyan') + colored("1", 'red') + colored("] Да", 'cyan'))
print(colored("╰", 'red') + colored("[", 'cyan') + colored("2", 'red') + colored("] Нет", 'cyan'))
print(colored("-------------------//", 'red'))
#------------------------------------------------------------
try:
    Vp = input(colored("Введите/> ", 'cyan'))
    if Vp not in ['1', '2']:
        raise ValueError(colored("Не корректно указан ответ!", 'red'))
except ValueError as e:
    print(e)
    exit()
    
if Vp == '1':
    os.system('termux-open-url https://t.me/DragonScrip')  
    execute_python_file("/data/data/com.termux/files/home/x0ne/documentation/Соглашение.py")
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
    clear_console()
if Vp == '2': 
    execute_python_file("/data/data/com.termux/files/home/x0ne/Documentation/Соглашение.py")
    def clear_console():
        os.system('cls' if os.name == 'nt' else 'clear')
    clear_console()